# shopping-list
A Shopping List app powered by Firebase and Nuxt!
## Build Setup

Be sure to create a .env file with the Firebase Config inside (check nuxt.config.js for the keys)

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

