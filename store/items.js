export const state = () => ({
  items: [],
})

export const getters = {
  uncompletedItems({ items }) {
    return items.filter((item) => !item.completed).map((i) => i.uid)
    // return items.map((doc) => doc.data()).sort((a, b) => a.created - b.created)
  },
  completedItems({ items }) {
    return items.filter((item) => item.completed).map((i) => i.uid)
  },
  items({ items }) {
    const sorted = [...items].sort((a, b) => (a.completed ? 1 : -1))
    return sorted
  },
}

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items
  },
  ADD_ITEM(state, item) {
    state.items.push(item)
  },
}
