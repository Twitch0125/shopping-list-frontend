export const state = () => ({
  user: '',
  showError: false,
  errorMsg: '',
  /**
   * @description this is a onSnapshot listener, to unsubscribe simply call the method.
   * @example this.$store.state.itemsListener()
   */
  itemsListener:null,
  /**
   * @description this is a onSnapshot listener, to unsubscribe simply call the method.
   * @example this.$store.state.usersListener()
   */
  usersListener:null,
  /**
   * should only be true on first login
   */
  redirectList: false,
  btn: {
    fn: null,
    text: null
  }
})

export const actions = {
  async nuxtServerInit({ dispatch }, { res }) {
    if (res?.locals?.user) {
      const { allClaims: claims, idToken: token, ...authUser } = res.locals.user
      await dispatch('onAuthStateChangedAction', {
        authUser,
        claims,
        token,
      })
    }
  },
  async onAuthStateChangedAction(
    { commit, dispatch, state },
    { authUser, claims }
  ) {
    if (authUser) {
      dispatch('registerListeners')
    }
    if (!authUser) {
      dispatch('cleanup')
      return
    }
    await this.$fire.firestore.collection('items').get()
    await this.$fire.firestore.collection('users').get()
    const { uid, email, emailVerified, displayName, photoURL } = authUser
    commit('SET_USER', { uid, email, emailVerified, displayName, photoURL })
  },
  cleanup({ commit, state }) {
    commit('SET_USER', {})
    if (typeof state.itemsListener === 'function') {
      state.itemsListener()
      
    }
    if (typeof state.usersListener === 'function') {
      state.usersListener()
    }
  },
  registerListeners({ commit }) {
    const unsubItems = this.$fire.firestore
      .collection('items')
      .onSnapshot((itemsQuery) => {
        commit(
          'items/SET_ITEMS',
          itemsQuery.docs
            .map((doc) => doc.data())
            .sort((a, b) => a.created - b.created)
        )
      })
    const unsubUsers = this.$fire.firestore
      .collection('users')
      .onSnapshot((itemsQuery) => {
        commit(
          'users/SET_USERS',
          itemsQuery.docs
            .map((doc) => doc.data())
            .sort((a, b) => a.created - b.created)
        )
      })
    commit('SET_ITEMS_LISTENER', unsubItems)
    commit('SET_USERS_LISTENER', unsubUsers)
  },
  async loginWithGoogle({ commit }) {
    const provider = new this.$fireModule.auth.GoogleAuthProvider()
    const response = await this.$fireModule.auth().signInWithPopup(provider)
    const { displayName, photoURL = '', email, uid } = response.user
    this.$fire.firestore
      .collection('users')
      .doc(uid)
      .set({ displayName, photoURL, email, uid })
    commit('SET_USER', { uid, email, displayName, photoURL })
    this.$router.push('/items')
  },
}

export const mutations = {
  SET_USER: (state, { uid, email, emailVerified, displayName, photoURL }) => {
    state.user = { uid, email, emailVerified, displayName, photoURL }
  },
  SET_MSG: (state, message) => {
    state.errorMsg = message
  },
  SET_SHOW_ERROR: (state, showError) => {
    state.showError = showError
  },
  SET_ITEMS_LISTENER: (state, listener) => {
    state.itemsListener = listener
  },
  SET_USERS_LISTENER: (state, listener) => {
    state.usersListener = listener
  },
  SET_REDIRECT_LIST: (state, val) => (state.redirectList = val),
  SET_BTN: (state, val) => state.btn = val
}

export const getters = {
  snackbar({ showError, errorMsg, btn }) {
    return { showError, errorMsg, btn}
  },
}
