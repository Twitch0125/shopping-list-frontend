export const state = () => ({
  users: [],
})

export const getters = {
  users({ users }) {
    return users
  },
}

export const mutations = {
  SET_USERS(state, users) {
    state.users = users
  },
  ADD_USER(state, user) {
    state.users.push(user)
  },
}
