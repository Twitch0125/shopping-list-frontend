import colors from "vuetify/lib/util/colors";
const light = {
  primary: colors.pink.base,
  accent: colors.pink.darken4,
  secondary: colors.pink.lighten3,
  info: colors.teal.lighten1,
  warning: colors.amber.base,
  error: colors.deepOrange.accent4,
  success: colors.green.base,
};
const dark = { ...light };

export default {
  loading: {
    color: "pink",
  },
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ssr: false,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "static",
  server: {
    port: process.env.PORT || 3000,
    host: process.env.HOST || "localhost",
  },
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Fira+Sans&display=swap",
      },
    ],
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ["~/plugins/sw.client.js"],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    // '@nuxtjs/tailwindcss',
    "@nuxtjs/vuetify",
    "@nuxtjs/dotenv",
    "@nuxtjs/pwa",
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // '@nuxtjs/auth',
    "@nuxtjs/svg",
    "@nuxtjs/firebase",
  ],
  firebase: {
    enablePersistence: true,
    config: {
      apiKey: process.env.API_KEY,
      authDomain: process.env.AUTH_DOMAIN,
      databaseURL: process.env.DATABASE_URL,
      projectId: process.env.PROJECT_ID,
      storageBucket: process.env.STORAGE_BUCKET,
      messagingSenderId: process.env.MESSAGING_SENDER_ID,
      appId: process.env.APP_ID,
      measurementId: process.env.MEASUREMENT_ID,
    },
    onFirebaseHosting: true,
    services: {
      auth: {
        persistence: "local", // default
        initialize: {
          // onAuthStateChangedMutation: 'ON_AUTH_STATE_CHANGED_MUTATION',
          onAuthStateChangedAction: "onAuthStateChangedAction",
        },
        ssr: true,
        emulatorPort: process.env.NODE_ENV === "development" ? 9099 : undefined,
      },
      firestore: {
        enablePersistence: true,
        synchronizeTabs: true,
        emulatorPort: process.env.NODE_ENV === "development" ? 8080 : undefined,
      },
    },
  },
  pwa: {
    icon: {
      source: "~/static/icon.png",
    },
    meta: {
      name: "Shopping List",
      theme_color: light.primary,
      nativeUI: true,
      mobileAppIOS: true,
      appleStatusBarStyle: "black",
    },
    workbox: {
      offlineAssets: ["_nuxt/*"],
    },
    manifest: {
      name: "Shopping List",
      // start_url: '/',
      background_color: light.primary,
    },
  },
  vuetify: {
    theme: {
      themes: {
        light,
        dark,
      },
    },
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
  generate: {
    fallback: true,
  },
};
