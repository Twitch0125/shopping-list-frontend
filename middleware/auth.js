export default function ({ redirect, store }) {
  if (!store.state.user.uid) {
    // await $fireModule.auth().signOut()
    redirect('/login')
  }
}
