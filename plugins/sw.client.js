export default async ({store}) => {
  if (process.env.NODE_ENV === 'production') {
    const wb = await window.$workbox
    if (wb) {
      
      wb.addEventListener('activated', (event) => {})
      wb.addEventListener('controlling', (event) => {})
      wb.addEventListener('installed', (event) => {
        if (event.isUpdate) {
          console.info('Update Available!');
          store.commit('SET_MSG', 'An update is available')
          store.commit('SET_SHOW_ERROR', true)
          store.commit('SET_BTN', {
            fn: () => {
              location.reload()
            },
            text: 'UPDATE'
          })
        }
      })
    }
  }
}
